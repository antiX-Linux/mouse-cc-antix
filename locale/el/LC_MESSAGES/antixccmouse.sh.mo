��          �   %   �      0     1  E   K     �     �     �     �     �     �  9   �  B   ,     o     �     �     �  	   �     �     �     �          %     7     H     [  �  v  5   "  �   X     �  1   �  0   0  3   a     �     �  �   �  �   \  (   	  '   +	  #   S	  :   w	     �	  W   �	  4   "
  4   W
  <   �
  (   �
     �
       R   &                               
                                              	                                                  Acceleration (Multiplier) After choosing a new theme, please\n logout/login to see the changes. Button Order Change cursor size Change cursor theme Change mouse motion Cursor Size Cursor Theme Cursor size changed. Please\nlogout/login to see changes. Default cursor size restored.\nPlease logout/login to see changes. Left hand layout Mouse Acceleration Mouse Options Mouse speed changed. No change Now using left-hand layout. Now using right-hand layout. Restore default motion Restore default size Right hand layout Size (in pixels) Threshold (Pixels) Using default mouse speed. Project-Id-Version: antix-development
Report-Msgid-Bugs-To: http://code.google.com/p/antix-development/issues
POT-Creation-Date: 2011-10-25 00:34+0300
PO-Revision-Date: 2011-11-20 18:05+0200
Last-Translator: anticapitalista <antiX@operamail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1)
 Επιτάχυνση (πολλαπλασιαστής) Αφού επιλέξετε ένα νέο θέμα, παρακαλούμε\ n logout /login για να δείτε τις αλλαγές. Σειρά κουμπιών Αλλαγή του δρομέα μεγέθους Επιλέξτε Δείκτη Ποντικιού Αλλαγή κίνηση του ποντικιού Δρομέας Μέγεθος Δρομέας Θέμα Δρομέας μέγεθος αλλάξει. Παρακαλούμε \nlogout/συνδεθείτε για να δείτε τις αλλαγές. Προκαθορισμένο μέγεθος δρομέα αποκατασταθεί. \ NΠαρακαλώ logout / login για να δείτε τις αλλαγές. Αριστερό χέρι διάταξη Επιτάχυνση Ποντικιού Επιλογές Ποντικιού Ταχύτητα του ποντικιού αλλάξει. Καμία αλλαγή Τώρα, χρησιμοποιώντας το αριστερό χέρι διάταξη. Τώρα με το δεξί χέρι διάταξη. Επαναφορά κίνηση προεπιλογή Επαναφορά προεπιλεγμένο μέγεθος Δικαίωμα διάταξη χέρι Μέγεθος (σε pixels) Κατώφλι (Pixel) Χρησιμοποιώντας default ταχύτητα του ποντικιού. 